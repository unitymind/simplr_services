# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'simplr_services/version'

Gem::Specification.new do |spec|
  spec.name           = 'simplr_services'
  spec.version        = SimplrServices::VERSION
  spec.authors        = ['Vitaliy V. Shopov']
  spec.email          = ['vitaliy.shopov@cleawing.com']
  spec.homepage       = 'http://simplr.ru'
  spec.summary        = 'External services.'
  spec.description    = 'External services.'
  spec.license        = 'MIT'

  spec.files          = `git ls-files`.split("\n")
  spec.test_files     = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths  = ['lib']

  spec.add_dependency 'google-api-client', '~> 0.7.1'
  spec.add_dependency 'activesupport', '~> 4.1.6'
  spec.add_dependency 'httparty', '~> 0.13.1'

  spec.add_development_dependency 'bundler', '~> 1.7'
  spec.add_development_dependency 'rake', '~> 10.0'

  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rspec-nc'
  spec.add_development_dependency 'guard'
  spec.add_development_dependency 'guard-rspec'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'pry-remote'
  spec.add_development_dependency 'pry-nav'
  spec.add_development_dependency 'vcr', '~> 2.9.3'
end
