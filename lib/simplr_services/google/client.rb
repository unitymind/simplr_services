require 'google/api_client'

module SimplrServices
  module Google
    class Client < ::Google::APIClient
      class << self
        def build(authorizer)
          new(
              application_name: 'SimplrServices',
              application_version: SimplrServices::VERSION,
              auto_refresh_token: true,
              authorization: authorizer,
              retries: 2
          )
        end
      end
    end
  end
end