module SimplrServices
  module Google
    class Analytics < SimplrServices::Google::Api
      IDENTITY = 'google.analytics.reader'

      class << self
        def build_from_token(token, options = {})
          super(token, 'analytics', 'v3', options)
        end
      end

      def fetch_account_summaries
        to_hash execute('management.account_summaries.list')
      end

      def fetch_accounts
        to_hash execute('management.accounts.list')
      end

      def fetch_properties(account_id = '~all')
        to_hash(
          execute('management.webproperties.list', {
              'accountId' => account_id
          })
        )
      end

      def fetch_profiles(account_id = '~all', web_property_id = '~all')
        to_hash(
          execute('management.profiles.list', {
              'accountId' => account_id,
              'webPropertyId' => web_property_id
          })
        )
      end

      def fetch_profile(account_id, web_property_id, profile_id)
        to_hash(
          execute('management.profiles.get', {
              'accountId' => account_id,
              'webPropertyId' => web_property_id,
              'profileId' => profile_id
          })
        )
      end
    end
  end
end