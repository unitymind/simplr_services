require 'httparty'

module SimplrServices
  module Google
    class Authorizer < SimplrServices::Oauth2::Authorizer

      AUTHORIZATION_URI = 'https://accounts.google.com/o/oauth2/auth'
      TOKEN_CREDENTIAL_URI = 'https://accounts.google.com/o/oauth2/token'

      def self.build(redirect_uri, client_id = nil, client_secret = nil)
        super(AUTHORIZATION_URI, TOKEN_CREDENTIAL_URI, client_id || ENV['GOOGLE_CLIENT_ID'], client_secret || ENV['GOOGLE_SECRET'], redirect_uri)
      end

      def self.build_from_token(token)
        authorizer = build(token.consumer.settings(:oauth2).redirect_uri)
        authorizer.fill_tokens_from(token)
        authorizer
      end

      def revoke!
        HTTParty.get("https://accounts.google.com/o/oauth2/revoke?token=#{self.access_token}") unless self.access_token.blank?
      end
    end
  end
end