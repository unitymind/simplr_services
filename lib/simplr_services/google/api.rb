module SimplrServices
  module Google
    class Api
      class << self
        def build_from_token(token, api_name, api_version = nil, options = {})
          authorizer = SimplrServices::Google::Authorizer.build_from_token(token)
          client = SimplrServices::Google::Client.build(authorizer)
          new(client, api_name, api_version, options)
        end
      end

      def initialize(client, api_name, api_version = nil, options = {})
        @client = client
        @cache_store = options.include?(:cache_store) ? options[:cache_store] : nil
        discover_api(api_name, api_version)
      end

      def discover_api(api_name, api_version = nil)
        if !@cache_store.nil?
          @api = @cache_store.fetch('%s/%s' % [api_name, api_version], expires_in: 1.day) do
            @client.discovered_api(api_name, api_version)
          end
        else
          @api = @client.discovered_api(api_name, api_version)
        end
      end

      def execute(method_string, params = {})
        @client.execute(api_method: build_method(method_string), parameters: params)
      end

      protected
        def build_method(method_chain_string)
          method_chain_string.split('.').inject(@api) {|o, a| o.send(a.to_sym) }
        end

        def to_hash(api_response)
          api_response.data.items.map { |item| item.to_hash.deep_symbolize_keys }
        end
    end
  end
end