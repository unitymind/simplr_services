module SimplrServices
  module Oauth2
    autoload :Authorizer, 'simplr_services/oauth2/authorizer'
  end
end