require 'signet/oauth_2/client'

module SimplrServices
  module Oauth2
    class Authorizer < ::Signet::OAuth2::Client
      class << self
        def build(authorization_uri, token_credential_uri, client_id, client_secret, redirect_uri)
          new(authorization_uri: authorization_uri,
              token_credential_uri: token_credential_uri,
              client_id: client_id,
              client_secret: client_secret,
              redirect_uri:  redirect_uri)
        end
      end

      def fill_tokens_from(token)
        self.access_token = token.access_token
        self.refresh_token = token.refresh_token
      end

      def fetch_by_code!(code, token)
        self.grant_type = 'authorization_code'
        self.code = code
        fetch_access_token_into!(token)
      end

      protected
        def fetch_access_token_into!(token)
          token_hash = self.fetch_access_token!
          token.access_token = token_hash['access_token']
          token.expires_at =  token_hash['expires_at']
          %w(refresh_token id_token).each do |key|
            token.send("#{key}=".to_sym, token_hash[key]) unless token_hash[key].blank?
          end
        end
    end
  end
end