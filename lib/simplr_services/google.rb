module SimplrServices
  module Google
    autoload :Analytics, 'simplr_services/google/analytics'
    autoload :Api, 'simplr_services/google/api'
    autoload :Authorizer, 'simplr_services/google/authorizer'
    autoload :Client, 'simplr_services/google/client'
  end
end