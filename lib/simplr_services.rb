require 'simplr_services/version'
require 'active_support/all'

module SimplrServices
  autoload :Oauth2, 'simplr_services/oauth2'
  autoload :Google, 'simplr_services/google'
end
