RSpec.describe SimplrServices::Google do
  describe 'Authorizer' do
    let(:callback_uri) { 'https://example.com/callback' }
    let(:scopes) { 'sample_scope' }

    it 'Ensure that really use ENV variables' do
      source = SimplrServices::Google::Authorizer.build(callback_uri)

      ENV['GOOGLE_CLIENT_ID'] = 'dummy_client_id'
      ENV['GOOGLE_SECRET'] = 'dummy_client_secret'

      modified = SimplrServices::Google::Authorizer.build(callback_uri)

      expect(source.authorization_uri.to_s).to_not eq modified.authorization_uri.to_s
    end
  end

end